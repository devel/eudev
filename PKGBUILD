# Maintainer: artoo <artoo@artixlinux.org>
# Contributor: Alexey D. <lq07829icatm@rambler.ru>
# Contributor: Ivailo Monev <xakepa10@gmail.com>

_ver=243

pkgbase=eudev
pkgname=('eudev' 'libeudev')
pkgver=3.2.12
pkgrel=2
pkgdesc="The userspace dev tools (udev) forked by Gentoo"
arch=('x86_64')
url="http://www.gentoo.org/proj/en/eudev/"
license=('GPL')
makedepends=('gobject-introspection' 'gperf' 'gtk-doc' 'intltool' 'hwdata'
            'kmod' 'libkmod.so' 'util-linux' 'libblkid.so' 'git' 'kbd')
options=('!libtool' '!staticlibs')
_commit=29cd296aa4635fe6f7b53bda2f2cb1648bdc0782
source=("$pkgname-$pkgver.tar.gz::https://github.com/gentoo/eudev/archive/v${pkgver}.tar.gz"
        initcpio-{hook,install}-udev
        git+https://gitea.artixlinux.org/artix/alpm-hooks.git#commit=$_commit
        udev-{hwdb-path,default-rules}.patch
        io-scheduler.default
        60-ioschedulers.rules)
sha256sums=('b3415b0da11294233506b39656653f459ae6a1593da7a1e4906c91ed18d17a5c'
            'ae33ca343056542fb3e77c07eb47c241581fad9f9149cfc5af5b8698967e8432'
            'acab362c5c7fe3c62da8dd80f0be3706db00841c2e9d6a311d5863da30d7ee2f'
            'SKIP'
            'e05e468d15d09fb49343f33bc8d21c08f54db3ecff412dc35d4acb0630f869a3'
            '13c167e448411b53b18532469e783e2e51a4c46ff4cf9c46e0481d6440b53f8d'
            'd2e2d1d989b5bf7d35339464c57b85f1f33f478b8913397b43da75486faac338'
            '7fee5bb7a70c7099041f9f01710137aef6ac572edf25ab6b921b17a3af3d0c27')

prepare(){
    cd "${pkgbase}-${pkgver}"
    #patch -Np 1 -i ${srcdir}/udev-hwdb-path.patch
    patch -Np 1 -i ${srcdir}/udev-default-rules.patch
    ./autogen.sh
}

build() {
    cd "${pkgbase}-${pkgver}"

    ./configure \
        --prefix=/usr \
        --with-rootprefix=/usr \
        --sysconfdir=/etc \
        --libdir=/usr/lib \
        --sbindir=/usr/bin \
        --bindir=/usr/bin \
        --enable-introspection \
        --enable-kmod \
        --enable-manpages \
        --enable-split-usr

    make
}

check(){
    cd "${pkgbase}-${pkgver}"
    make -k check
}

package_eudev() {
    pkgdesc="The userspace dev tools (udev) forked by Gentoo"
    depends=('libeudev' 'kbd' 'kmod' 'libkmod.so' 'bash' 'glibc' 'gcc-libs'
            'hwdata' 'util-linux' 'libblkid.so')
    provides=("udev=${_ver}")
    conflicts=('udev')
    backup=('etc/udev/udev.conf')

    cd "${pkgbase}-${pkgver}"

    make DESTDIR="${pkgdir}" install

    install -dm755 ${srcdir}/_libeudev

    mv -v ${pkgdir}/usr/lib/libudev*.{so*,a} ${srcdir}/_libeudev

    # io-scheduler
    install -Dm644 ../io-scheduler.default "${pkgdir}"/etc/default/io-scheduler
    install -m644 ../60-ioschedulers.rules "${pkgdir}"/usr/lib/udev/rules.d/

    # initcpio
    install -D -m0644 ../initcpio-install-udev "${pkgdir}"/usr/lib/initcpio/install/udev
    install -D -m0644 ../initcpio-hook-udev "${pkgdir}"/usr/lib/initcpio/hooks/udev

    # pacman hooks
#     cd alpm-hooks
    make -C ../alpm-hooks DESTDIR="${pkgdir}" install_udev
}

package_libeudev() {
    pkgdesc="eudev client libraries"
    depends=('glibc' 'gcc-libs')
    provides=('libudev.so' 'libudev')
    conflicts=('libudev')

    install -dm755 ${pkgdir}/usr/lib
    mv -v ${srcdir}/_libeudev/libudev*.{so*,a} ${pkgdir}/usr/lib
}
